<h2>{{ isset($key) ? 'Авто ' . ($key + 1) : 'Добавить' }}</h2>
<div class="input-group mb-3">
    <div class="form-group">
        <label for="mark">Марка</label>
        <input id="mark" type="text" class="form-control" name="cars[{{ isset($car) ? $car->id : 'create' }}][mark]" value="{{ isset($car) ? $car->mark : '' }}">
    </div>
    <div class="form-group">
        <label for="model">Модель</label>
        <input id="model" type="text" class="form-control" name="cars[{{ isset($car) ? $car->id : 'create' }}][model]" value="{{ isset($car) ? $car->model : '' }}">
    </div>
    <div class="form-group">
        <label for="color">Цвет кузова</label>
        <input id="color" type="text" class="form-control" name="cars[{{ isset($car) ? $car->id : 'create' }}][color]" value="{{ isset($car) ? $car->color : '' }}">
    </div>
    <div class="form-group">
        <label for="license_plate">Гос. номер</label>
        <input id="license_plate" type="text" class="form-control" name="cars[{{ isset($car) ? $car->id : 'create' }}][license_plate]" value="{{ isset($car) ? $car->license_plate : '' }}">
    </div>
</div>