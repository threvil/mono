@extends('layout')

@section('title', 'Список Клиентов')

@section('content')

<h1 class="float-left">Все клиенты</h1>
<a href="{{ route('client.profile.get') }}" class="float-right clearfix">Создать</a>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <td>ФИО</td>
        <td>Авто</td>
        <td>Гос. номер</td>
        <td colspan="2">Управление</td>
      </tr>
    </thead>
    <tbody>
      @foreach($cars as $key => $car)
        <tr>
            <td>{{ $car->client->name }}</td>
            <td>Авто {{ $key + 1 }}</td>
            <td>{{ $car->license_plate }}</td>
            <td><a href="{{ route( 'client.profile.get', [$car->client_id] ) }}">Edit</a></td>
            <td>
              <form action="{{ route( 'client.profile.delete', [$car->client_id]) }}" method="post">
                {{ csrf_field() }}  
                {{method_field('DELETE')}}
                <button type="submit">Remove</button>
              </form>
            </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
{{ $cars->links() }}

@endsection