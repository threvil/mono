@extends('layout')

@section('title', (@isset( $client ) ? 'Обновление' : 'Создание') . ' профиля клиента')

@section('content')
<a class="float-right" href="{{ route('clients') }}">Все клиенты</a>
<h1>Профиль Клиента</h1>
<form action="{{ route('client.profile.post', [$client ? $client->id : '']) }}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">ФИО</label>
        <input id="name" type="text" class="form-control" name="name" value="{{ $client ? $client->name : '' }}">
    </div>
    <div class="form-group">
        <label>Пол</label>
        <div class="form-check">
            <input id="male" class="form-check-input" type="radio" name="gender" value="male" {{ $client && $client->gender == 'male' ? 'checked' : '' }}>
            <label class="form-check-label" for="male">
                М
            </label>
        </div>
        <div class="form-check">
            <input id="female" class="form-check-input" type="radio" name="gender" value="female" {{ $client && $client->gender == 'female' ? 'checked' : '' }}>
            <label class="form-check-label" for="female">
                Ж
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="phone">Телефон</label>
        <input id="phone" type="text" class="form-control"  name="phone" value="{{ $client ? $client->phone : '' }}">
    </div>
    <div class="form-group">
        <label for="address">Адрес</label>
        <input id="address" type="text" class="form-control"  name="address" value="{{ $client ? $client->address : '' }}">
    </div>

    <button class="btn btn-primary mb-3">Сохранить</button>
    
    @foreach($cars as $key => $car)
        @include('cars.profile')
    @endforeach
    @unset($car, $key)
    @include('cars.profile')

    <button class="btn btn-primary">Сохранить</button>
</form>


@endsection