<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('clients', 'ClientsController@index')->name('clients');
Route::get('profile/{id?}', 'ClientsController@showProfileForm')->name('client.profile.get');
Route::post('profile/{id?}', 'ClientsController@profile')->name('client.profile.post');
Route::delete('profile/{id}', 'ClientsController@delete')->name('client.profile.delete');