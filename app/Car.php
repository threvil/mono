<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = ['mark', 'model', 'color', 'license_plate', 'client_id'];
    public function client(){
        return $this->belongsTo('App\Client');
     }
}
