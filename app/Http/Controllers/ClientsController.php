<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Client;
use App\Car;

class ClientsController extends Controller
{
    // список всех клиентов
    public function index(Request $request){
        // Eager Loading
        $cars = Car::with('client')->paginate();

        return view('clients', ['cars' => $cars]);
    }
    public function showProfileForm( $client_id = null ){
        $client = $this->find($client_id);

        $cars = DB::table('cars')->where('client_id', $client_id)->get();

        return view('profile', ['client' => $client, 'cars' => $cars]);
    }
    public function profile(Request $request, $client_id = null){
        $client = DB::table('clients');

        $request->validate(array_merge($this->fields($client_id)['client'], $this->fields($client_id)['car']));

        DB::transaction(function () use ($client_id, $client, $request){
            $data_client  = $request->only( array_keys($this->fields()['client']) );
            // добавялем "хвост" в зависимости от того, обновляется или создается профиль
            if ($client_id) {
                $client->where('id', $client_id)->update($data_client);
            } else {
                $client_id = $client->insertGetId($data_client);
            }
            $this->cars($request->cars, $client_id);
        });
        return $this->showProfileForm($client_id);
    }
    public function delete(Request $request, int $client_id = null){
        $client = $this->find($client_id)->delete();

        return $this->index($request);
    }
    protected function fields( $client_id = null ){
        return [
            'client' => [
                'name'   => 'required|min:3|max:100|string',
                'gender' => 'required|in:male,female',
                'phone'  => 'required|unique:clients,phone,'.$client_id,
                'address' => '',
            ],
            'car' => [
                'cars.*.mark' => 'required',
                'cars.*.model' => 'required',
                'cars.*.color' => 'required',
                'cars.*.license_plate' => 'required|unique:cars',
            ],
        ];
    }
    private function find( $client_id = null ){
        // для возможности использования в методах showProfileForm и delete
        $client = Client::find( $client_id );
        // если передан id, но клиента нет, значит ссылка неверная
        if ($client_id && !$client) {
            abort(404);
        }
        return $client;
    }
    private function cars($cars, $client_id) {
        if (!empty($cars['create'])) {
            DB::table('cars')->insert(array_merge($cars['create'],['client_id' => $client_id]));
        }
    }
}
