<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Client extends Model
{
    protected $fillable = ['name', 'gender', 'phone', 'address'];
    public static function boot() {
        parent::boot();
    
        static::deleting(function($client) {
            DB::table('cars')->where('client_id', $client->id)->delete();
        });
    }
}
