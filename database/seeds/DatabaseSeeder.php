<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        factory(App\Client::class, 50)->create()->each(function ($client) {
            $client->cars()->save(factory(App\Car::class)->make());
        });
    }
}
