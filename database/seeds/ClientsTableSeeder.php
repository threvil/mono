<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'name'   => Str::random(5) . ' ' . Str::random(4) . ' ' . Str::random(5), 
            'gender' => array(['male', 'female'])[mt_rand(0,1)], 
            'phone'  => '+7 ' . mt_rand(9000000000, 9999999999), 
            'address'=> Str::random(5) . ' St.',
        ]);
    }
}
