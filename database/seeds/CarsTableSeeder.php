<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'name'   => Str::random(5) . ' ' . Str::random(4) . ' ' . Str::random(5), 
            'gender' => array(['male', 'female'])[mt_rand(0,1)], 
            'phone'  => '+7 ' . mt_rand(9000000000, 9999999999), 
            'address'=> Str::random(5) . ' St.',
            'mark'   => Str::random(5), 
            'model'  => Str::random(6), 
            'color'  => Str::random(10), 
            'license_plate' => Str::random(5), 
        ]);
        
    }
}
